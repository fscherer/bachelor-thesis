% Evaluates a stream of optic flow events against the adp frames of the DVS


% input 
% Flow-Event Stream, .flo Datei und timestampsarray der Frames
flowStreamFile = 'C:/...' ;
floPattern = 'C:/Users/Flo/Desktop/Daten/quadrat/groundTruth/gtFlo_%04d.flo';
tsFile = 'C:/Users/Flo/Desktop/Daten/quadrat/groundTruth/timestampsGT.txt';


% Output
% Endpoint Error: length of the difference vector
% angular Error: the angle between the two normalized vectors

%%%%%%%%%%%%
% placeholder
FlowStream = load(flowStreamFile);
xVec = FlowStream(:,1);
yVec = FlowStream(:,2);
vxVec = FlowStream(:,3); % vx and vy should be in pixels/ms
vyVec = FlowStream(:,4);
tsVec = FlowStream(:,5);

tsFrames = load(tsFile);
nrGroundTruths = size(tsFrames,1) - 1;

% matrices which store all errors, maybe omitted later for performance
errMatrixEP = zeros(180, 240, nrGroundTruths);
errMatrixAE = zeros(180, 240, nrGroundTruths);
% for calculating average Errors over each Frame;
errSumEP = zeros(nrGroundTruths,1);
errSumAE = zeros(nrGroundTruths,1);


for i = 1 : nrGroundTruths
    
    groundTruth = readFlowFile(sprintf(floPattern,i));
    vxTruth = groundTruth(:,:,1);
    vyTruth = groundTruth(:,:,2);
    
    vxMatrix = zeros(180,240);
    vyMatrix = zeros(180,240);  

    % find over which Events to integrate
    tsA = find(tsVec > tsFrames(i),1);
    tsB = find(tsVec > tsFrames(i+1),1);
    
    % add up all events into Matrix (integrate/interpolate etc...)
    for j = tsA : tsB
        x = xVec(j);
        y = yVec(j);
        
        vxMatrix(y,x) = vxMatrix(y,x) + vxVec(j);
        vyMatrix(y,x) = vyMatrix(y,x) + vyVec(j); 
    end
    
    % bring GT of pixels/frame into pixels/Millisecond
    deltaTS = (tsFrames(i+1) - tsFrames(i))/1000;
    vxTruth = vxTruth / deltaTS;
    vyTruth = vyTruth / deltaTS;
    
    
    % for loop to have sparse error metrics
    errCount = 0.;
    for j = 1 : 180
        for k = 1 : 240
            if (vxMatrix(j,k) ~= 0) || (vyMatrix(j,k) ~= 0)
                
                errCount = errCount + 1;
                
                % Calc EP
                errEP = sqrt((vxMatrix(j,k) - vxTruth(j,k))^2 + (vyMatrix(j,k) - vyTruth(j,k))^2);
                errMatrixEP(j,k,i) = errEP;
                errSumEP(i) = errSumEP(i) + errEP;
                
                % normalize
                length = sqrt(vxMatrix(j,k)^2 + vyMatrix(j,k)^2);
                vxMatrix(j,k) = vxMatrix(j,k) / length;
                vyMatrix(j,k) = vyMatrix(j,k) / length;
                
                length = sqrt(vxTruth(j,k)^2 + vyTruth(j,k)^2);
                vxTruth(j,k) = vxTruth(j,k) / length;
                vyTruth(j,k) = vyTruth(j,k) / length;
                
                % Calc AE
                errAE = acos(vxMatrix(j,k)*vxTruth(j,k) + vyMatrix(j,k)*vyTruth(j,k));
                errMatrixAE(j,k,i) = errAE;
                errSumAE(i) = errSumAE(i) + errAE;
            end
        end
    end
    
    % save average error metrics for this Ground Truth
    errSumAE(i) = errSumAE(i) / errCount;
    errSumEP(i) = errSumEP(i) / errCount;
    
end

% plotting
errAverageAE = mean(errSumAE);
errAverageEP = mean(errSumEP);

subplot(2,1,1)
plot(errSumAE);
title('Angular Error')
ylabel('error in rad')
axis([1 21 0.5 2])


subplot(2,1,2)
plot(errSumEP);
title('Endpoint Error')
xlabel('Frames')
ylabel('error in px/ms')
axis([1 21 0.5 2])


% implay(errMatrixAE)
% implay(errMatrixEP)


