function [xVec,yVec,polVec,tsVec] = cleaner(xVec,yVec,polVec,tsVec)

% deletes events from broken pixels

nrEvents = size(xVec,1);


x = 0;
y = 0;

j = 0; % tracked how many rows got erased and adapts the index

for i = 1 : nrEvents % erases all events from "broken" pixels
    
    x = xVec(i - j);
    y = yVec(i - j);
    
    % this is for the DavisB nr 1
    if ((x == 1)&&(y == 1)) || ((x == 24)&&(y == 96)) || ((x == 25)&&(y == 95)) || ((x == 25)&&(y == 96)) || ((x == 33)&&(y == 66))|| ((x == 168)&&(y == 91)) 
        
        xVec(i - j) = [];
        yVec(i - j) = [];
        polVec(i - j) = [];
        tsVec(i - j) = [];
        
        j = j + 1;
        
    end
    
end
    
end

