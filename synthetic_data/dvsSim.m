clear
timerstart = cputime;

% script that generates synthetic DVS Data from us-Frames and saves it as a
% workspace and aedat file

% location of frames is specified in the rendEvents function

firstFrame = 0; % better not change this
nrBatches = 199;
batchSize = 500;

%frames per second that are being evaluated
fps = 1000000;

%threshold for triggering an event
thresh = 10/255;  % adapt to diff calculation and file format

tsStart = 124143; % arbitrary start timestamp, same as in EXR2FLO!!


xVec = [];
yVec = [];
polVec = [];
tsVec = [];

for batch = 1 : nrBatches
    
    firstFrameBatch = (batch - 1) * batchSize + firstFrame;
    lastFrameBatch = batch * batchSize - 1 + firstFrame;
   
    [xVecTmp, yVecTmp, polVecTmp, tsVecTmp] = rendEvents(firstFrameBatch, lastFrameBatch, thresh, fps, tsStart);
    
    xVec = [xVec; xVecTmp];
    yVec = [yVec; yVecTmp];
    polVec = [polVec; polVecTmp];
    tsVec = [tsVec; tsVecTmp];
    
end

timer = cputime - timerstart;

% diese Zeilen verhinderen den read Error vom aedat file
while yVec(1) < 144 && yVec(1) > 139
    yVec(1) = [];
    xVec(1) = [];
    polVec(1) = [];
    tsVec(1) = [];
end

% create .aedat file
genRawAddr = @(x,y,pol) (2^22*y + (240-1-x)*2^12 + (1-pol)*2^11);

countEv = size(xVec,1);
train = zeros(countEv,2,'uint32');

for i = 1 : countEv
    train(i,1) = uint32(tsVec(i));
    train(i,2) = uint32(genRawAddr(xVec(i),yVec(i),polVec(i)));
end

saveaerdat(train,'baelle_sim.aedat');
mat = [xVec,yVec,polVec,tsVec];
dlmwrite('baelle_sim',mat,'precision','%10f')



 
 
 