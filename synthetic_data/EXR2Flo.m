% This script concatinates two .exr flow fields to one .flo flow field to
% handle the moving edges problem


clear

startGT = 1;
endGT = 200; %number of GT frames in .exr format

tsStart = 0; %arbitrary start timestamp, has to be the same in dvsSim
% better double check if the first image of the rendering process and the
% first image of the ground truth are the same, maybe tsStart needs to be
% altered

fps = 1000; %fps of GT

% Input
exrPattern = 'C:/Users/Flo/Desktop/Daten/blender_occlusion/groundTruth/gtEXR_%04d.exr';
% Output
floPattern = 'C:/Users/Flo/Desktop/Daten/blender_occlusion/groundTruth/gtFlo_%04d.flo';
tsFile = 'C:/Users/Flo/Desktop/Daten/blender_occlusion/timestampsGT.txt';


% don't use first and last GT because they sometimes get dropped
for i = startGT + 1 : endGT - 1 
    
    % take gt between frame i-1 and frame i from frame i-1
    exrName = sprintf(exrPattern, i-1);
    
    [im,mask] = exrread(exrName);
    
    vx1 = im(:,:,3);
    vy1 = mask;
    
    % take gt between frame i-1 and frame 1 from frame i
    exrName = sprintf(exrPattern, i);
    
    [im,mask] = exrread(exrName);
    
    vx2 = im(:,:,1);
    vy2 = im(:,:,2);
    
    v1 = sqrt(vx1 .* vx1 + vy1 .* vy1);
    v2 = sqrt(vx2 .* vx2 + vy2 .* vy2);
    
    % concatinate the two gt

    size_y = size(vx1,1);
    size_x = size(vx1,2);

    gt = zeros(size_y, size_x,2);
    
    for row = 1 : size_y
        for col = 1 : size_x
            
            if v1(row,col) ~= 0 && v2(row,col) ~= 0
                
                if vx1(row,col) == vx2(row,col) && vy1(row,col) == vy2(row, col)
                    gt(row,col,1) = vx1(row,col);
                    gt(row,col,2) = vy2(row,col);
                    
                else
                    
                    % at points of occlusion: check which part of the flow
                    % field remains seen and which disappears behind the
                    % other
                    
                    % we either find the flow of v1 in v2 or the other way
                    % round
                    
                    % calculate where to look and clamp to bounds
                    find1in2_row = max( min(row + int32(vy1(row,col)), size_y), 1);
                    find1in2_col = max( min(col + int32(vx1(row,col)), size_x), 1);
                    
                    
                    find2in1_row = max( min(row - int32(vy2(row,col)), size_y), 1);
                    find2in1_col = max( min(col - int32(vx2(row,col)), size_x), 1);
                    
                    
                    % calculate weights, instead of just choosing the more
                    % similar flow, so the case of fast nonlinear motion is
                    % approximated
                    
                    err1x = vx1(row,col) - vx2(find1in2_row, find1in2_col);
                    err1y = vy1(row,col) - vy2(find1in2_row, find1in2_col);
                    err1 = sqrt(err1x * err1x + err1y * err1y);
                    
                    err2x = vx2(row,col) - vx1(find2in1_row, find2in1_col);
                    err2y = vy2(row,col) - vy1(find2in1_row, find2in1_col);
                    err2 = sqrt(err2x * err2x + err2y * err2y);
                    
                    
                    % wenn beide Fehler = 0 sind dann ist ecke einer 90� verschiebung
                    % hierf�r gibt es noch keine L�sung
                    
                    % vorschlag: mittelwert
                    
                    
                    if err1 < 0.1 && err2 < 0.1
                        weight1 = 0.5;
                        weight2 = 0.5;
                    else
                        weight1 = err2 / (err1 + err2);
                        weight2 = err1 / (err1 + err2);
                    end
                    
                    gt(row,col,1) = weight1 * vx1(row,col) + weight2 * vx2(row,col);
                    gt(row,col,2) = weight1 * vy1(row,col) + weight2 * vy2(row,col);
                    
                    
                    %                 if err1 > err2
                    %                     gt(row,col,1) = vx2(row,col);
                    %                     gt(row,col,2) = vy2(row,col);
                    %                 elseif err2 > err1
                    %                     gt(row,col,1) = vx1(row,col);
                    %                     gt(row,col,2) = vy1(row,col);
                    %                 else
                    %                     gt(row,col,1) = (vx1(row,col) + vx2(row,col)) / 2;
                    %                     gt(row,col,2) = (vy1(row,col) + vy2(row,col)) / 2;
                    %                 end
                    
                    
                end
                
            elseif v1(row,col) ~= 0
                gt(row,col,1) = vx1(row,col);
                gt(row,col,2) = vy1(row,col);
            elseif v2(row,col) ~= 0
                gt(row,col,1) = vx2(row,col);
                gt(row,col,2) = vy2(row,col);
            end
        end
    end
    
    gt(:,:,1) = -gt(:,:,1);

    floName = sprintf(floPattern, i - 1);
    writeFlowFile(gt,floName);

end

% write timecode file of frames

timestamps = tsStart : 1000000/fps : tsStart + 1000000/fps*(endGT-startGT);
timestamps = transpose(timestamps);

dlmwrite(tsFile,timestamps, 'precision', 10);


