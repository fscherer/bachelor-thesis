% script to generate ground truth for the geometric figures with linear
% movement

% vx and vy in pixels/ms

clear

% -1 when right to left and +1 when left to right
firstLine = 74;
lastLine = 164;

% dist = ;
% angularVel = ;

% positive from left to right - from x = 0 to x = 240
velX = -(120/325);
% negative from y = 0 to y = 180 - positive from up to down
velY = (5/325);

ts = zeros(2,1);
ts(1) = 398246877;
ts(2) = 398666844;

% file with the gt timestamps
tsFile = 'Daten/geometrie/trapez_16cm_10rpm/timestampsGT.txt';
floFile = 'Daten/geometrie/trapez_16cm_10rpm/gtFlo_0001.flo';

gt = zeros(180,240,2);

for y = firstLine : lastLine
   for x = 1 : 240
      gt(y,x,1) = velX;
      gt(y,x,2) = velY;
   end
end



writeFlowFile(gt,floFile);
dlmwrite(tsFile,ts, 'precision', 10);

[x,y] = meshgrid(1:240,1:180);

u = gt(:,:,1);
v = gt(:,:,2);

figure
quiver(x,y,flipud(u),flipud(-v),1/2)
axis([0 240 0 180]);



