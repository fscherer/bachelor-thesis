function servoRotate()

% 0-4095 is a full turn

portName = 'COM5';
baudRate = 1000000;
servoId = 006;
firstPosition = 2500;
secondPosition = 0;


% *************************************************************************


SerPortCloseAll();
ser=SerPortOpen(portName, baudRate);      % open serial port

setPosition(ser, servoId, firstPosition);
pause(5)
setPosition(ser, servoId, secondPosition);


SerPortCloseAll();
disp('Done');

return



function setPosition(ser, servoId, goalPosition)
   
instruction = 3; % motor comand instruction ID
lowestByte = mod(goalPosition, 256);
highestByte = floor(goalPosition / 256);
parameters = [30, lowestByte, highestByte];
nParameters = length(parameters);
packetLength = nParameters + 2;
checkSum = servoId + packetLength + instruction + sum(parameters);
checkSum = 255 - mod(checkSum, 256);
checkSum = checkSum
packet = [255, 255, servoId, packetLength, instruction, parameters, checkSum];

fwrite(ser, packet,'uint8');

return




% *************************************************************************
function s=SerPortOpen(portName, baudRate)      % open specified port
    s = serial(portName, ...
           'BaudRate', baudRate, ...
           'Parity', 'none', ...
           'DataBits', 8, ...
           'StopBits', 1);
    fopen(s);                           % finally open the serial port
    disp ('COM-Port open');
    return
    
    
function SerPortCloseAll()                      % close all open ports
    if ~(isempty(instrfind))
        fclose(instrfind);
    end
    delete(instrfindall);
    return
