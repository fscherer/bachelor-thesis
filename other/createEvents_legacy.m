clear
timerstart = cputime;

% old simulation, now as script dvsSim and func rendEvents

% takes a stream of frames, compares them to each other and outputs a
% stream of events

% maybe polarity is the wrong way around (white has highest intensity)

% the frames have to be associated with a timestamp, so that a timestamp for
% each event can be calculated

Frames = readImgSeq('Daten/blender_baelle/Frames/%04d.png',1,400);

%frames per second that are being evaluated
fps = 1000000;

%threshold for triggering an event
thresh = 10/255;  % adapt to diff calculation and maybe connect with fps

tsStart = 124143; % arbitrary start timestamp

n = 30000; % max nr Events
nrFrames = size(Frames,3); %nr frames
countEv = 0;

% preallocate for n events
xVec = zeros(n,1);
yVec = zeros(n,1);
polVec = zeros(n,1);
tsVec = zeros(n,1);

% timestamps for m frames
tsFrames = tsStart : (1000000/fps) : tsStart + (1000000*nrFrames/fps);

% propability for noise of random on-events: on average one per 25 seconds
chanceNoise = 1. / (25. * double(fps));

for frame = 2 : nrFrames
    
    for x = 1 : size(Frames,2)
        for y = 1 : size(Frames,1)
            
            I2 = Frames(y,x,frame);
            I1 = Frames(y,x,frame-1);
            
            diff = I2-I1;
            % diff = (I2-I1)/I1;
            % implementation of TCON in paper
            % to speed up -> implement a table (also)
            % diff = ln(I2) - ln(I1);
            
            if diff > thresh
                
                % positive event
                countEv = countEv + 1;
                xVec(countEv) = x;
                yVec(countEv) = 180 - y;
                tsVec(countEv) = tsFrames(frame);
                %polVec(countEv) = 0; % not necessary
                
            elseif diff < -thresh
                
                % negative event
                countEv = countEv + 1;
                xVec(countEv) = x;
                yVec(countEv) = 180 - y;
                tsVec(countEv) = tsFrames(frame);
                polVec(countEv) = 1; % no idea why
                
            else
                % noise of rand on events
                if (rand < chanceNoise)
                    
                    countEv = countEv + 1;
                    xVec(countEv) = x;
                    yVec(countEv) = 180 - y;
                    tsVec(countEv) = tsFrames(frame);
                    %polVec(countEv) = 0;
                    
                end
                
            end
            
        end
    end
    
end

xVec = xVec(1:countEv);
yVec = yVec(1:countEv);
polVec = polVec(1:countEv);
tsVec = tsVec(1:countEv);

timer = cputime - timerstart;

% create .aedat file
genRawAddr = @(x,y,pol) (2^22*y + (240-1-x)*2^12 + (1-pol)*2^11);

train = zeros(countEv,2,'uint32');

for i = 1 : countEv
    train(i,1) = uint32(tsVec(i));
    train(i,2) = uint32(genRawAddr(xVec(i),yVec(i),polVec(i)));
end

saveaerdat(train,'Daten/blender_ball/ball_sim2.aedat');



 
 
 