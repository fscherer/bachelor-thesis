% this script generates the .flo files and timestampsGT.txt files for the
% ground truth of the pinwheel datasets

% first clockwise than counterclockwise

tsFile = 'Daten/pinwheel/gro�_b&f/groundTruth/counterclockwise/timestampsGT.txt';
floFile = 'Daten/pinwheel/gro�_b&f/groundTruth/counterclockwise/gtFlo_0001.flo';

midpointX = 120;
midpointY = 90;
radius = 56;

w = - 0.00825; % rad/ms  ; negative for counterclockwise, positive for clockwise

ts = zeros(2,1);

ts0 = 32519177; % start of aedat file
ts1 = ts0 + 880000; % start of clockwise turn
ts2 = ts0 + 1584000; % end of clockwise turn
ts3 = ts0 + 2658000; % start of counterclockwise turn
ts4 = ts0 + 3326000; % end of counterclockwise turn

ts(1) = ts3; %ts1, ts2 for clockwise turn, and ts3, ts4 for counterclockwise
ts(2) = ts4;

gtFrame = zeros(180,240,2);

for row = 1 : 180
    for col = 1 : 240
        rx = col - midpointX;
        ry = row - midpointY;
        if sqrt((rx)^2 + (ry)^2) <= radius
            gtFrame(row,col,1) = - w * ry;
            gtFrame(row,col,2) = w * rx;
        end
        
    end
end

writeFlowFile(gtFrame, floFile);
dlmwrite(tsFile,ts, 'precision', 10);

[x,y] = meshgrid(1:240,1:180);
quiver(x,y,flipud(gtFrame(:,:,1)), flipud( - gtFrame(:,:,2)));
