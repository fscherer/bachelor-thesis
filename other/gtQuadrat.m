% ground truth for the dataset was manually labeled and stored in the
% flow.txt file. This script writes the .flo Files

v = load('Daten/quadrat/flow.txt');

floPattern = 'Daten/quadrat/groundTruth/gtFlo_%04d.flo';

colStart = 70;
colEnd = 120;

for i = 1 : size(v,1)
    
    gtFrame = zeros(180,240,2);
    
    
    colEnd = colEnd + ceil(v(i,1)); 
    % end column will be updated before gt calc and start column afterwards
    % because of moving edges problem
    
   for row = 87 : 131
       for col = colStart : colEnd
           
           gtFrame(row,col,1) = v(i,1);
           gtFrame(row,col,2) = v(i,2);
           
       end
   end
   
   colStart = colStart + floor(v(i,1));
   
   writeFlowFile(gtFrame, sprintf(floPattern,i));
   
end