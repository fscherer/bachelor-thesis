function [v] = velocitySimple(tsE, tsP, tsM)

% takes the three timestamps Event, Plus and Minus
% calculates the velocity of one direction

% deltaX defined as a constant --> result is pixels per millisecond
deltaX = 1000;

% does not have to be good/perfect/stable
% the others write a better one

if (tsM == 0) || (tsM == tsE)
    
    v1 = 0;
    
else
    
    v1 = deltaX / (tsE - tsM);
    
end

if (tsP == 0)  || (tsP == tsE)
    
    v2 = 0;
    
else
    
    v2 = deltaX / (tsE - tsP);
    
end


v = v1 - v2;

% cheat! but 5 pixels per milisecond is ridiculously high for that data set
if abs(v) > 5
    v = 0;
end


end