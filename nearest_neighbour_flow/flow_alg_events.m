% simple nearest neighbor flow algorithm, not tested and not used in BA

% input Event Stream
% Output Flow Stram (vx,vy and x,y,ts of Input)

% array has the same nr of rows as input
% if there is no flow event yet, simply output (0,0)

clear
[xVec,yVec,polVec,tsVec] = getDVSeventsDavis('quadrat_kurz_DVS_Only.aedat');

% shift index from c array to matlab
nrEvents = size(xVec,1);
for i = 1:nrEvents
    xVec(i) = xVec(i) + 1;
    yVec(i) = yVec(i) + 1;
end

% erase all broken pixels
[xVec,yVec,polVec,tsVec] = cleaner(xVec, yVec, polVec, tsVec);

nrEvents = size(xVec,1);

% allocating
MatrixEventsON = zeros(180, 240);
MatrixEventsOFF = zeros(180,240);

vxVec = zeros(nrEvents,1);
vyVec = zeros(nrEvents,1);

vx = 0.;
vy = 0.;

pol = 0;
ts = 0;
x = 0;
y = 0;

% go through all events and output flow Event

for i = 1 : nrEvents % can be edited for certain time windows (adapt to frame timestamps)
    
    pol = polVec(i);
    ts = tsVec(i);
    x = xVec(i);
    y = yVec(i);
    
    if pol == 1
        
        MatrixEventsON(y,x) = ts;
        
        if (y>1) && (y<180) && (x>1) && (x<240)
            
            vx = velocitySimple(MatrixEventsON(y,x), MatrixEventsON(y,x+1), MatrixEventsON(y,x-1));
            vy = velocitySimple(MatrixEventsON(y,x), MatrixEventsON(y+1,x), MatrixEventsON(y-1,x));
            
        else 
            vx = 0.;
            vy = 0.;
        end
        
    else
        
        MatrixEventsOFF(y,x) = ts;
        
        if (y>1) && (y<180) && (x>1) && (x<240) 
            
            vx = velocitySimple(MatrixEventsOFF(y,x), MatrixEventsOFF(y,x+1), MatrixEventsOFF(y,x-1));
            vy = velocitySimple(MatrixEventsOFF(y,x), MatrixEventsOFF(y+1,x), MatrixEventsOFF(y-1,x));
            
        else
            
            vx = 0.;
            vy = 0.;
        
        end
    end
    
    vxVec(i) = vx;
    vyVec(i) = vy;
    
end




