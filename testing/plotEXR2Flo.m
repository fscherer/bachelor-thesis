testCase = 6;
% 1: schr�g quadrat
% 2: rechtwinkling quadrat
% 3: kreis schr�g
% 4: rechtwinklig v = 2
% 5: quadrat v = 2
% 6: kreis gerade



if testCase == 1
    % % schr�g
    vx1 = [-1 -1 -1 -1 -1 -1; -1 -1 -1 -1 -1 -1;  -1 1 1 1 -1 -1; -1 1 1 1 -1 -1; -1 1 1 1 -1 -1; -1 -1 -1 -1 -1 -1 ];
    vy1 = [0 0 0 0 0 0; 0 0 0 0 0 0 ; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 0 0 0 0 0];
    
    vx2 = [-1 -1 -1 -1 -1 -1; -1 -1 1 1 1 -1;  -1 -1 1 1 1 -1; -1 -1 1 1 1 -1; -1 -1 -1 -1 -1 -1; -1 -1 -1 -1 -1 -1 ];
    vy2 = [0 0 0 0 0 0; 0 0 -1 -1 -1 0 ; 0 0 -1 -1 -1 0; 0 0 -1 -1 -1 0; 0 0 0 0 0 0; 0 0 0 0 0 0];
    
elseif testCase == 2
    % % rechtwinklig
    vx1 = [-1 -1 -1 -1 -1 -1; -1 -1 -1 -1 -1 -1;  -1 0 0 0 -1 -1; -1 0 0 0 -1 -1; -1 0 0 0 -1 -1; -1 -1 -1 -1 -1 -1 ];
    vy1 = [0 0 0 0 0 0; 0 0 0 0 0 0 ; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 0 0 0 0 0];
    
    vx2 = [-1 -1 -1 -1 -1 -1;  -1 0 0 0 -1 -1; -1 0 0 0 -1 -1; -1 0 0 0 -1 -1; -1 -1 -1 -1 -1 -1; -1 -1 -1 -1 -1 -1];
    vy2 = [0 0 0 0 0 0; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 -1 -1 -1 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0];
    
elseif testCase == 3
    % % kreis schr�g
    vx1 = [0 0 0 0 0 0; 0 0 1 0 0 0; 0 1 1 1 0 0; 1 1 1 1 1 0; 0 1 1 1 0 0; 0 0 1 0 0 0];
    vy1 = [0 0 0 0 0 0; 0 0 -1 0 0 0; 0 -1 -1 -1 0 0; -1 -1 -1 -1 -1 0; 0 -1 -1 -1 0 0; 0 0 -1 0 0 0];
    
    vx2 = [0 0 0 1 0 0; 0 0 1 1 1 0; 0 1 1 1 1 1; 0 0 1 1 1 0; 0 0 0 1 0 0; 0 0 0 0 0 0];
    vy2 = [0 0 0 -1 0 0; 0 0 -1 -1 -1 0; 0 -1 -1 -1 -1 -1; 0 0 -1 -1 -1 0; 0 0 0 -1 0 0; 0 0 0 0 0 0];
elseif testCase == 4
    % % rechtwinklig v = 2
    
    vy1 = zeros(8);
    vx1(1:8, 1:8) = -1;
    vy1(5:8, 3:6) = -2;
    vx1(5:8, 3:6) = 0;
    
    vy2 = zeros(8);
    vx2(1:8, 1:8) = -1;
    vy2(3:6, 3:6) = -2;
    vx2(3:6, 3:6) = 0;
    
    
elseif testCase == 5
    % % quadrat v = 2
    
    vy1 = zeros(8);
    vx1(1:8, 1:8) = -1;
    vx1(3:5, 1:3) = 2;
    
    vy2 = zeros(8);
    vx2(1:8, 1:8) = -1;
    vx2(3:5, 3:5) = 2;
    
elseif testCase == 6
    % Kreis gerade
    
    sz = 15;

    vx1 = zeros(sz);
    vy1 = zeros(sz);
    mid = (sz / 2) ;
    radius = 5;
    vyc = 0;
    vxc = 2;
    
    vx2 = zeros(sz);
    vy2 = zeros(sz);
    
    for row = 1 : sz
        for col = 1 : sz
            
            if sqrt((row - mid)^2 + (col - mid)^2) < radius
                vx1(row - vyc  ,col - vxc) = 2*vxc;
                vy1(row - vyc  ,col - vxc) = 2*vyc;
                
                vx2(row + vyc, col + vxc) = 2*vxc;
                vy2(row + vyc, col + vxc) = 2*vyc;
            end
            
        end
    end
    
end

v1 = sqrt(vx1 .* vx1 + vy1 .* vy1);
v2 = sqrt(vx2 .* vx2 + vy2 .* vy2);

% concatinate the two gt

size_y = size(vx1,1);
size_x = size(vx1,2);

gt = zeros(size_y, size_x,2);


for row = 1 : size_y
    for col = 1 : size_x
        
        if v1(row,col) ~= 0 && v2(row,col) ~= 0
            
            if vx1(row,col) == vx2(row,col) && vy1(row,col) == vy2(row, col)
                gt(row,col,1) = vx1(row,col);
                gt(row,col,2) = vy2(row,col);
                
            else
                
                % at points of occlusion: check which part of the flow
                % field remains seen and which disappears behind the
                % other
                
                % we either find the flow of v1 in v2 or the other way
                % round
                
                % calculate where to look and clamp to bounds
                find1in2_row = max( min(row + int32(vy1(row,col)), size_y), 1);
                find1in2_col = max( min(col + int32(vx1(row,col)), size_x), 1);
                
                
                find2in1_row = max( min(row - int32(vy2(row,col)), size_y), 1);
                find2in1_col = max( min(col - int32(vx2(row,col)), size_x), 1);
                
                
                % calculate weights, instead of just choosing the more
                % similar flow, so the case of fast nonlinear motion is
                % approximated
                
                err1x = vx1(row,col) - vx2(find1in2_row, find1in2_col);
                err1y = vy1(row,col) - vy2(find1in2_row, find1in2_col);
                err1 = sqrt(err1x * err1x + err1y * err1y);
                
                err2x = vx2(row,col) - vx1(find2in1_row, find2in1_col);
                err2y = vy2(row,col) - vy1(find2in1_row, find2in1_col);
                err2 = sqrt(err2x * err2x + err2y * err2y);
                
                
                % wenn beide Fehler = 0 sind dann ist ecke einer 90� verschiebung
                % hierf�r gibt es noch keine L�sung
                
                % vorschlag: mittelwert
                
                
                if err1 < 0.1 && err2 < 0.1 
                    weight1 = 0.5;
                    weight2 = 0.5;
                else
                    weight1 = err2 / (err1 + err2);
                    weight2 = err1 / (err1 + err2);
                end
                
                gt(row,col,1) = weight1 * vx1(row,col) + weight2 * vx2(row,col);
                gt(row,col,2) = weight1 * vy1(row,col) + weight2 * vy2(row,col);
                

%                 if err1 > err2
%                     gt(row,col,1) = vx2(row,col);
%                     gt(row,col,2) = vy2(row,col);
%                 elseif err2 > err1
%                     gt(row,col,1) = vx1(row,col);
%                     gt(row,col,2) = vy1(row,col);
%                 else
%                     gt(row,col,1) = (vx1(row,col) + vx2(row,col)) / 2;
%                     gt(row,col,2) = (vy1(row,col) + vy2(row,col)) / 2;
%                 end

                
            end
            
        elseif v1(row,col) ~= 0
            gt(row,col,1) = vx1(row,col);
            gt(row,col,2) = vy1(row,col);
        elseif v2(row,col) ~= 0
            gt(row,col,1) = vx2(row,col);
            gt(row,col,2) = vy2(row,col);
        end
    end
end

[x,y] = meshgrid(1:size_x, 1:size_y);

figure

subplot(1,3,2)
quiver(x,y,flipud(gt(:,:,1)),flipud(-gt(:,:,2)),1/2)
axis([0 size_x+1 0 size_y+1])
title('GT')
set(gca,'YTick',[]);
set(gca,'XTick',[]);

subplot(1,3,1)
quiver(x,y,flipud(vx1),flipud(-vy1),1/2)
axis([0 size_x+1 0 size_y+1])
title('v1')
set(gca,'YTick',[]);
set(gca,'XTick',[]);

subplot(1,3,3)
quiver(x,y,flipud(vx2),flipud(-vy2),1/2)
axis([0 size_x+1 0 size_y+1])
title('v2')
set(gca,'YTick',[]);
set(gca,'XTick',[]);