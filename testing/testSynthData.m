% tests the Event Stream of an synthetic Dataset for y values between 139
% and 144 of the first event -> these trigger a bug in the saveaerdat
% script

clear
load('eventstream.mat');

% diese Schleife auskommentieren um Error zu erzeugen
while yVec(1) < 144 && yVec(1) > 139
    yVec(1) = [];
    xVec(1) = [];
    polVec(1) = [];
    tsVec(1) = [];
end

% create .aedat file
genRawAddr = @(x,y,pol) (2^22*y + (240-1-x)*2^12 + (1-pol)*2^11);

countEv = size(xVec,1);
train = zeros(countEv,2,'uint32');

for i = 1 : countEv
    train(i,1) = uint32(tsVec(i));
    train(i,2) = uint32(genRawAddr(xVec(i),yVec(i),polVec(i)));
end

saveaerdat(train,'test.aedat');

[xTest, yTest, polTest, tsTest] = getDVSeventsDavis('test.aedat');

errX = find(xVec - xTest);
errY = find(yVec - yTest);
errPol = find(polVec - polTest);
errTs = find(tsVec - double(tsTest));
