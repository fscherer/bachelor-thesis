% tests the function genRaw addr by loading xVec, yVec etc from a working
% dataset, creating a new train, saving it and then compare the written
% file to the original data

clear

genRawAddr = @(x,y,pol) (2^22*y + (240-1-x)*2^12 + (1-pol)*2^11);

[xVec, yVec, polVec, tsVec] = getDVSeventsDavis('Daten/quadrat/quadrat_DVS.aedat');


[addr, ts] = loadaerdat('Daten/quadrat/quadrat_DVS.aedat');

sz = size(xVec,1);

addrNew = zeros(sz,1,'uint32');

for i = 1 : sz
   
    addrNew(i) = uint32(genRawAddr(xVec(i), yVec(i), polVec(i)));
    
end

errorsAddr = find(addr- addrNew);


train = tsVec;
train(:,2) = addrNew;

saveaerdat(train, 'test.aedat');

[xVec2, yVec2, polVec2, tsVec2] = getDVSeventsDavis('test.aedat');

diffX = find(xVec-xVec2);
diffY = find(yVec-yVec2);
diffPol = find(polVec - polVec2);
diffTs = find(tsVec - tsVec2);
