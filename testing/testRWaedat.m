% tests the script loadaerdat.m, saveaerdat.m, und getDVSeventsDavis.m by
% loading a working dataset, saving it, loading the saved file and compare
% it with the inital one
clear 

[addr, ts] = loadaerdat('Daten/quadrat/quadrat_DVS.aedat');

train = ts;
train(:,2) = addr;

saveaerdat(train, 'test.aedat');

[addr2, ts2] = loadaerdat('test.aedat');

[x, y, pol, tsD] = getDVSeventsDavis('Daten/quadrat/quadrat_DVS.aedat');
[x2, y2, pol2, tsD2] = getDVSeventsDavis('test.aedat');

diffAddr = find(addr - addr2);
diffTs = find(ts - ts2);

diffX = find(x - x2);
diffY = find(y - y2);
diffPol = find(pol - pol2);
difftsD = find(tsD - tsD2);
