% plot EXR flo


% [flo, mask] = exrread('C:\Users\Flo\Desktop\Daten\blender_quadrat_1\groundTruth\gtEXR_0001.exr');
% 
% u1 = - flo(:,:,3);
% v1 = mask;
% 
% u2 = - flo(:,:,1);
% v2 = flo(:,:,2);
% 
% sz = size(v1);
% [x,y] = meshgrid(1:sz(2),1:sz(1));
% 
% figure
% 
% subplot(1,2,2)
% quiver(x,y,flipud(u1),flipud(-v1),1/2)
% axis([0 sz(2) 0 sz(1)]);
% title('flo(3) and mask')
% 
% subplot(1,2,1)
% quiver(x,y,flipud(u2),flipud(-v2),1/2)
% axis([0 sz(2) 0 sz(1)]);
% title('flo(1) and flo(2)')



% plot flo file

% img = readFlowFile('C:\Users\Flo\Desktop\Daten\pushbot\groundTruth\5-0-0\frame00004_mdpof.flo');
img = readFlowFile('C:\Users\Flo\Desktop\Daten\Pushbot\groundTruth\5-0-0\frame00009_mdpof.flo');

sz = size(img);

% [x,y] = meshgrid(1:sz(2),1:sz(1));
u = img(:,:,1);
v = img(:,:,2);

% figure
% quiver(x,y,flipud(u),flipud(-v),1/2)
% axis([0 sz(2) 0 sz(1)]);

% average over 4x4 cells for readability

u2 = zeros(36,48);
v2 = zeros (36,48);

[x,y] = meshgrid(1:48,1:36);

for i = 0 : 35
   for j = 0 : 47
       
       u2(i+1, j + 1) = 0;
       v2(i+1, j + 1) = 0;
       
       for i2 = 1 : 5
           for j2 = 1 : 5
               u2(i+1, j+1) = u (i * 5 + i2, j * 5 + j2);
               v2(i+1, j+1) = v (i * 5 + i2, j * 5 + j2);
           end
       end
       
       u2(i+1, j + 1) = u2(i+1, j + 1) / 25;
       v2(i+1, j + 1) = v2(i+1, j + 1) / 25;
       
   end
end

figure
quiver(x,y,flipud(u2),-flipud(v2))
axis([0 48 0 36]);