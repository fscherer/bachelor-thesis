% integrate over timeframe

vxMatrix = zeros(180,240);
vyMatrix = zeros(180,240);


  %change for loop to match timestamp of frames
for i = 1 : nrEvents
    
    vxMatrix(yVec(i),xVec(i)) = vxMatrix(yVec(i),xVec(i)) + vxVec(i);
    vyMatrix(yVec(i),xVec(i)) = vyMatrix(yVec(i),xVec(i)) + vyVec(i);
    
end


% % calculate angle
% 
% angleMatrix = zeros(180,240);
% 
% for i = 1 : 180
%     for j = 1 : 240
%         if vxMatrix(i,j) ~= 0
%             angleMatrix(i,j) = atan(vyMatrix(i,j)/vxMatrix(i,j));
%         elseif vyMatrix ~= 0
%             angleMatrix(i,j) = pi/2;
%         end
%     end
% end


%           plotting


[x,y] = meshgrid(1:240,1:180);

figure
quiver(x,y,vxMatrix,vyMatrix)
axis([0 240 0 180]);



%averaging version

% u2 = zeros(45,60);
% v2 = zeros (45,60);
% 
% for i = 0 : 44
%    for j = 0 : 59
%        
%        u2(i+1, j + 1) = 0;
%        v2(i+1, j + 1) = 0;
%        
%        for i2 = 1 : 4
%            for j2 = 1 : 4
%                u2(i+1, j+1) = vxMatrix(i * 4 + i2, j * 4 + j2);
%                v2(i+1, j+1) = vyMatrix(i * 4 + i2, j * 4 + j2);
%            end
%        end
%        
%        u2(i+1, j + 1) = u2(i+1, j + 1) / 16;
%        v2(i+1, j + 1) = v2(i+1, j + 1) / 16;
%        
%    end
% end
% 
% figure
% quiver(x,y,u2,v2)
% axis([0 60 0 45]);