% just random testing/scribbling, older things I might still need are just
% commented out
% 
[xVec,yVec,polVec,tsVec] = getDVSeventsDavis('Daten/pinwheel/gro�_b&f/gro�_b&f.aedat');

% 
% counter = zeros(180,240);
% 
% for i = 1 : size(tsVec,1)
%    counter(yVec(i)+1, xVec(i)+1) = counter(yVec(i)+1, xVec(i)+1)+1; 
% end
% 
% imshow(counter);

% [train, ts] = loadaerdat('Daten/geometrie/ball_13cm_7rpm.aedat');

% [train,ts] = loadaerdat('Daten/blender_quadrat_2/quadrat_2.aedat');


% [flo, mask] = exrread('C:\Users\Flo\Desktop\Daten\blender_expansion\groundTruth\gtEXR_0002.exr');
% 
% img1_x = - flo(:,:,3);
% img1_y = mask;
% 
% [flo, mask] = exrread('C:\Users\Flo\Desktop\Daten\blender_expansion\groundTruth\gtEXR_0025.exr');
% img2_x = - flo(:,:,3);
% img2_y = mask;
% 
% [x,y] = meshgrid(1:240, 1:180);
% 
% figure
% subplot(1,2,1)
% quiver(x,y,flipud(img1_x), flipud(-img1_y))
% title('exr02')
% 
% subplot(1,2,2)
% quiver(x,y,flipud(img2_x), flipud(-img2_y))
% title('exr25')

% tests wthe Event Stream of an synthetic Dataset

% clear
% load('Daten/blender_baelle/Eventstream.mat');
% xVec = xVec - 1;
% 
% % diese Zeile verhindert den Error
% % if yVec(1) < 144 && yVec(1) > 139
% %     yVec(1) = [];
% %     xVec(1) = [];
% %     polVec(1) = [];
% %     tsVec(1) = [];
% % end
% 
% % create .aedat file
% genRawAddr = @(x,y,pol) (2^22*y + (240-1-x)*2^12 + (1-pol)*2^11);
% 
% countEv = size(xVec,1);
% train = zeros(countEv,2,'uint32');
% 
% errors = zeros(180,240,2);
% 
% 
% for k = 0 : 239
%     for j = 0 : 179
%         for l = 0 : 1
%             
%             xVec(1) = k;
%             yVec(1) = j;
%             polVec(1) = l;
%             
%             for i = 1 : countEv
%                 train(i,1) = uint32(tsVec(i));
%                 train(i,2) = uint32(genRawAddr(xVec(i),yVec(i),polVec(i)));
%             end
%             
%             saveaerdat(train,'test.aedat');
%             
%             [xTest, yTest, polTest, tsTest] = getDVSeventsDavis('test.aedat');
%             
%             if(size(xTest,1) < countEv)
%                 errors(j + 1,k + 1, l + 1) = 1;
%             end
%         end
%     end
% end

% [xVec, yVec, polVec, tsVec] = getDVSeventsDavis('Daten/blender_baelle/baelle_sim.aedat');
% 
% mat = zeros(180,240);
% 
% for i = 1 : size(xVec,1)
%     mat(yVec(i)+1, xVec(i)+1) = 1;  
% end
% 
% imshow(mat);

% tmp = zeros(180,240);
% for i = 1 : 253
%    tmp(yVec(i),xVec(i)) = 1;
% end
%
% imwrite(tmp,'temp.png');
%

% count = 0;
% for i = 1 : 252
%     if tsVec(i) == tsVec(i+1)
%         count = count + 1;
%     end
% end

% a = [];
%
% for i = 1 : 100
%    a = [1 ;a];
% end


% videoObj = VideoWriter('errorMovieEP.avi');
%
% open(videoObj)
%
% for k = 1:20
%     writeVideo(videoObj, errMatrixEP(:,:,k))
% end
%
% close(videoObj);



%angucken bestimmter Flow Files
% [img] = readFlowFile('temp/quadrat/scene00002_mdpof.flo');
% u = flipud(img(:,:,1));
% v = flipud(img(:,:,2));
%
% [x,y] = meshgrid(1:240,1:180);
%
% quiver(x,y,u,v)

% [img] = readFlowFile('./old00002_mdpof.flo');
%
% u_old = flipud(img(:,:,1));
% v_old = flipud(img(:,:,2));
%


%[xVec,yVec,polVec,tsVec] = getDVSeventsDavis('quadrat_kurz_DVS_Only.aedat');
%[xVecSim,yVecSim,polVecSim,tsVecSim] = getDVSeventsDavis('Daten/blender_ball/ball_sim.aedat');

% count = 0;
% countSim = 0;
%
% for i = 1 : size(tsVec) - 1
%     if tsVec(i) == tsVec(i+1)
%         count = count + 1;
%     end
% end
%
% for i = 1 : size(tsVecSim) - 1
%    if tsVecSim(i) == tsVecSim(i+1)
%        countSim = countSim + 1;
%    end
% end


% for i = 1:180
%     for j = 1:240
%         for k = 1:20
%             if (errMatrixEP(i,j,k) > 1.)
%                 errMatrixEP(i,j,k) = 1.;
%             end
%         end
%     end
% end


% subplot(2,1,1)
% plot(errSumAE)
% title('Angular Error')
% ylabel('error in rad')
% axis([0 21 errAverageAE-0.2 errAverageAE+0.2])
%
%
% subplot(2,1,2)
% plot(errSumEP)
% title('Endpoint Error')
% xlabel('Frames')
% ylabel('error in px/ms')
% axis([0 21 errAverageEP-1 errAverageEP+1])





