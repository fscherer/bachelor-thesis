
# Datasets and a unified systems for the evaluation of optic flow from event based data #

**This repository contains most of the code from my Bachelor Thesis with the above-mentioned title** (pdf available per inbox)

The most important scripts are:

- EvalFrame.m: this one reads in a stream of flow events and evaluates it against ground truth

- dvsSim.m: Takes in a series of rendered frames to create synthetic event based data (CUDA Version [here](https://github.com/rochus/eventgen))

- EXR2FLO.m: Concatinates two .EXR flow fields to one .flo flow field (Moving Edges problem)

the rest are scripts either to read/write certain dataformats, for testing, for visualization or simply to try out stuff

The scripts for handling the AER event based data are copies from [here](https://sourceforge.net/p/jaer/code/HEAD/tree/scripts/matlab/)

The scripts for handling the .flo files are from [here](http://vision.middlebury.edu/flow/submit/)

For .exr files I used [these](http://www.mit.edu/~kimo/software/matlabexr/)


--> better always double check the coordinate system since in computer vision the pixels are usually numbered starting from 0,0 in the upper left corner, the DVS starts with 0,0 in the lower left and matlab always starts with 1,1